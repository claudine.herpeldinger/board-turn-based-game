/** Class representant le plateau */
class GameBoard {
    /**
     *
     * Création du plateau
     *
     * @param rows - ligne du plateau
     * @param  cols - Colonne du plateau
     * @param map - La map
     */
    constructor(map, rows, cols) {

        this.rows = rows;
        this.cols = cols;
        this.map = document.getElementById(map);
    }


    /**
     *displayGame
     *
     * Methode qui crée le plateau
     *
     */
    displayGame() {
        this.map.innerHTML = "";

        let cell = [];

        for (let i = 0; i < this.rows; i++) {

            cell[i] = [];

            let divRow = document.createElement("div");
            this.map.appendChild(divRow);
            divRow.classList.add("divRowTable");

            for (let j = 0; j < this.cols; j++) { // on cree nos colonnes

                let divCol = divRow.appendChild(document.createElement("div"));
                divCol.classList.add("divColTable");

                cell[i][j] = new Cellule(String(i) + "-" + String(j), "empty", i, j, false);

                $(".divColTable").attr("id", cell[i][j].id);
                $(".divColTable").attr("class", cell[i][j].classe);

                if (cell[i][j].isAccess === false) {

                    ((cell[i][j]).classe) = "cell-grey";
                    $("#" + cell[i][j].id).attr("class", cell[i][j].classe);

                }

            }

        }

        return cell;
    }

    /**
     *getEmptyCell
     *
     * Methode qui récupère les cellules libres
     *
     * @return elementEmpty
     *
     */
    getEmptyCell() {
        var elementEmpty = null;
        var i = 0;

        while(true) {
            var x = Math.floor(Math.random()*9);
            var y = Math.floor(Math.random()*9);
            elementEmpty = document.getElementById(x + '-' + y);
            if (elementEmpty.className == 'empty') {
                break;
            }
            if (i > 1000) {
                elementEmpty = null;
                break;
            }
            i++;
        }

        return elementEmpty
    }

}



let play = new GameBoard("map",10,10);
play.displayGame();










